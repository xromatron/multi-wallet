import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MultiWallet {
    private static final String CURRENCY_RATE_CONFIG = "currency.cfg";

    public static void main(String[] args) {
        final Map<String, Double> wallet = new ConcurrentHashMap<>();
        final Map<String, Double> currencyRates = new HashMap<>();

        if (args.length == 1) {
            try {
                var fileReader = new Reader(new FileReader(args[0], StandardCharsets.UTF_8), wallet);
                System.out.println("Loading data from " + args[0]);
                fileReader.run();
            } catch (IOException e) {
                System.out.println("File not found: " + args[0]);
            }
        }

        try {
            var fileReader = new Reader(new FileReader(CURRENCY_RATE_CONFIG, StandardCharsets.UTF_8), currencyRates);
            System.out.println("Loading currency rates from " + CURRENCY_RATE_CONFIG);
            fileReader.run();
        } catch (IOException e) {
            System.out.println("File with currency rates not found: " + args[0]);
        }

        var consolePrinter = new Printer(wallet, currencyRates);
        var printer = new Thread(consolePrinter);
        printer.start();

        var consoleReader = new Reader(new InputStreamReader(System.in), wallet);
        consoleReader.run();

        printer.interrupt();

    }

}
