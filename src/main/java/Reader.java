import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class Reader {
    final private InputStreamReader isReader;
    final private Map<String, Double> storage;

    public Reader(InputStreamReader isReader, Map<String, Double> storage) {
        this.storage = storage;
        this.isReader = isReader;
    }

    public void run() {
        String line;
        try (var reader = new BufferedReader(isReader)) {
            while ((line = reader.readLine()) != null) {
                if (line.equals("quit")) {
                    break;
                }
                if (line.matches("[A-Z]{3}\\s-?\\d+(.\\d+)?")) {
                    String key = line.substring(0, 3);
                    Double value = Double.valueOf(line.substring(4));
                    storage.compute(key, (k, v) -> v == null ? value : v + value);
                } else {
                    System.out.println("Incorrect format: " + line + ". Example: \"USD 1000.00\"");
                }
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
