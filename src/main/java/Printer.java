import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Printer implements Runnable {

    private final int DELAY_IN_SECONDS = 10;
    final private Map<String, Double> wallet;
    final private Map<String, Double> currency;
    private volatile boolean stopped;

    public Printer(Map<String, Double> wallet, Map<String, Double> currency) {
        this.wallet = wallet;
        this.currency = currency;
    }

    @Override
    public void run() {
        while (!stopped) {
            try {
                if (!wallet.isEmpty()) {
                    System.out.println("\nBalances:");
                    wallet.entrySet().stream().filter(es -> es.getValue() != 0)
                            .forEach(this::print);
                }
                Thread.sleep(TimeUnit.SECONDS.toMillis(DELAY_IN_SECONDS));
            } catch (InterruptedException e) {
                stopped = true;
            }
        }
    }

    private void print(Map.Entry<String, Double> entry) {
        System.out.printf("%s %.0f", entry.getKey(), entry.getValue());

        Double rate = currency.get(entry.getKey());
        if (rate != null) {
            System.out.printf(" (USD %.2f)", entry.getValue() / rate);
        }
        System.out.println();
    }

}
